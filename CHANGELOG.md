## 0.0.6 (2022-10-26)

### fixed (1 change)

- [fix bugs](tng.tarasenko/changelog_project@8b7b25113cc74d0971359707cf608b65729c3e93)

## 0.0.5 (2022-10-26)

### fixed (1 change)

- [fix bugs](tng.tarasenko/changelog_project@8b7b25113cc74d0971359707cf608b65729c3e93)

## 0.0.4 (2022-10-26)

### added (1 change)

- [Add feature file](tng.tarasenko/changelog_project@71cfedde9b7c0d7d6393c9b06a8abc1576e891c0) ([merge request](tng.tarasenko/changelog_project!1))

## 0.0.3 (2022-10-26)

### fixed (1 change)

- [fix some bug](tng.tarasenko/changelog_project@020c8e6c59d935de9a1c113049cf3cc0499726a9)

### feature (1 change)

- [New feature](tng.tarasenko/changelog_project@c9103ac9ec4df8bcae7657672f5f36263716580d)

## 0.0.2 (2022-10-26)

### fixed (1 change)

- [fix some bug](tng.tarasenko/changelog_project@020c8e6c59d935de9a1c113049cf3cc0499726a9)

## 0.0.1 (2022-10-26)

### feature (1 change)

- [New feature](tng.tarasenko/changelog_project@c9103ac9ec4df8bcae7657672f5f36263716580d)
